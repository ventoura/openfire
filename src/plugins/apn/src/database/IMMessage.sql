-- $Revision$
-- $Date$

INSERT INTO ofVersion (name, version) VALUES ('IMMessage', 0);

CREATE TABLE IF NOT EXISTS IMMessage(
	packetId VARCHAR(48), time VARCHAR(48),
	fromUsername VARCHAR(128),
	toUsername VARCHAR(128),
	body VARCHAR(2048)
);

ALTER TABLE IMMessage add primary key(packetId);

create INDEX packetIndex ON IMMessage(packetId);
