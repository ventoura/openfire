<%@ page
	import="java.io.File,
                 java.util.List,
                 java.util.HashMap,
                 java.util.Map,
                 org.jivesoftware.openfire.XMPPServer,
                 org.jivesoftware.util.*,
                 com.ventoura.openfire.plugin.apn.APNPlugin,
                 org.apache.commons.io.FileUtils,
                 org.apache.commons.fileupload.FileItem,
                 org.apache.commons.fileupload.disk.DiskFileItemFactory,
                 org.apache.commons.fileupload.servlet.ServletFileUpload,
                 org.apache.commons.fileupload.FileUploadException"
	errorPage="error.jsp"%>

<%
	// Get parameters
	boolean save = request.getParameter("save") != null;
	boolean success = request.getParameter("success") != null;
	boolean error = request.getParameter("error") != null;
	String password ="";
	String sound = "default";
	String keyStorePath = "";
	String production = "false";

	APNPlugin plugin = (APNPlugin) XMPPServer.getInstance()
			.getPluginManager().getPlugin("apn");

	// Handle a save
	if (save) {
		try {
			List<FileItem> multiparts = new ServletFileUpload(
					new DiskFileItemFactory()).parseRequest(request);
			Map<String, String> commonField = new HashMap<String, String>();
			File keyStoreFile = File.createTempFile("keystore",".p12");
			for (FileItem item : multiparts) {
				if (!item.isFormField()) {
					String filename = item.getName();
					item.write(keyStoreFile);
				}else{
					// Process regular form field (input
					// type="text|radio|checkbox|etc", select, etc).
					String fieldname = item.getFieldName();
					String fieldvalue = item.getString("UTF-8").trim();
					commonField.put(fieldname, fieldvalue);
				}
			}
			
			if(commonField.containsKey("password")) {
				password = commonField.get("password");
			}
			if(commonField.containsKey("sound")){
				sound = commonField.get("sound");
			}
			if(commonField.containsKey("production")){
				production = commonField.get("production");
			}
			if(commonField.containsKey("keyStorePath")){
				keyStorePath = commonField.get("keyStorePath");
			}
			File keyStoreFileFoler = new File(keyStorePath);
			if(!keyStoreFileFoler.exists()){
				keyStoreFileFoler.mkdirs();
			}
			// the name here should be the same used in the APNPlugin 
			FileUtils.copyFile(keyStoreFile, new File(keyStorePath + "/iosKeyStore.p12"));

			plugin.setPassword(password);
			plugin.setSound(sound);
			plugin.setProduction(production);
			plugin.setKeyStorePath(keyStorePath);
			
			response.sendRedirect("index.jsp?success=true");
			return;
		} catch (Exception e) {
			response.sendRedirect("index.jsp?error=true");
			return;
		}

	}

	password = plugin.getPassword();
	sound = plugin.getSound();
	production = plugin.getProduction() ? "true" : "false";
%>

<html>
<head>
<title>APNS Settings Properties</title>
<meta name="pageID" content="apn-settings" />
</head>
<body>

	<%
		if (success) {
	%>
	<div class="jive-success">
		<table cellpadding="0" cellspacing="0" border="0">
			<tbody>
				<tr>
					<td class="jive-icon"><img src="images/success-16x16.gif"
						width="16" height="16" border="0"></td>
					<td class="jive-icon-label">APNS certificate updated
						successfully.</td>
				</tr>
			</tbody>
		</table>
	</div>
	<br>
	<%
		}
	%>

	<form action="index.jsp?save" method="post"
		enctype="multipart/form-data">

		<div class="jive-contentBoxHeader">APNS certificate</div>
		<div class="jive-contentBox">
			<label for="file">p12 certificate:</label> 
			<input type="file" name="file" /> <br> 
			<label for="password">certificate password:</label> 
			<input type="password" name="password" value="<%=password%>" /> <br> 
			<label for="keyStorePath">Keystore Path</label>
			<input type="text" name="keyStorePath" /> <br> 
			<label for="sound">payload sound</label>
			<input type="text" name="sound" value="<%=sound%>" /> <br> 
			<label for="production">sandbox or production</label> 
			<input type="radio" name="production" value="false" <%=production.equals("true") ? "" : "checked"%>>Sandbox
			<input type="radio" name="production" value="true" <%=production.equals("true") ? "checked" : ""%>>Production
		</div>
		<input type="submit" value="Save">
	</form>


</body>
</html>
