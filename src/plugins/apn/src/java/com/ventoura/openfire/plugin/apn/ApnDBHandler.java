package com.ventoura.openfire.plugin.apn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.jivesoftware.database.DbConnectionManager;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApnDBHandler {

	private static final Logger Log = LoggerFactory.getLogger(APNPlugin.class);

	private static final String LOAD_TRAVELLER_TOKEN = "SELECT osType, deviceToken FROM Traveller WHERE id=?";
	private static final String LOAD_GUIDE_TOKEN = "SELECT osType,deviceToken FROM Guide WHERE id=?";
	private static final String LOAD_TRAVELLER_MOBILE_VERSION = "SELECT mobileVersionCode FROM Traveller WHERE id=?";
	private static final String LOAD_GUIDE_MOBILE_VERSION = "SELECT mobileVersionCode FROM Guide WHERE id=?";
	
	
	private static final String LOAD_SENDER_TRAVELLER_NAME = "SELECT travellerFirstname FROM Traveller WHERE id=?";
	private static final String LOAD_SENDER_GUIDE_NAME = "SELECT guideFirstname FROM Guide WHERE id=?";
	private static final String LOAD_GUIDE_MESSAGE_ALERT = "SELECT messageAlert FROM GuideSetting where guideId=?";
	private static final String LOAD_TRAVELLER_MESSAGE_ALERT = "SELECT messageAlert FROM TravellerSetting where travellerId=?";

	private static final String NUMBER_OF_OFFLINE_MESSAGE = "SELECT count(*) FROM ofOffline where username = ?";

	private static final String IS_TGMATCH_FRIEND = "SELECT count(*) FROM TGMatch where guideId=? and travellerId=? and guideAttitude=1 and travellerAttitude=1";
	private static final String IS_TTMATCH_FRIEND = "SELECT count(*) FROM TTMatch where blueTravellerId=? and redTravellerId=? and blueAttitude=1 and redAttitude=1 ";

	private static Connection databaseConnection = null;

	public int getNumberOfOfflineMessage(JID userJID) {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String numberOfOfflineMessages = null;
		try {
			databaseConnection = DbConnectionManager.getConnection();
			pstmt = databaseConnection
					.prepareStatement(NUMBER_OF_OFFLINE_MESSAGE);
			pstmt.setString(1, userJID.getNode());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				numberOfOfflineMessages = rs.getString(1);
			}
		} catch (SQLException sqle) {
			Log.error(sqle.getMessage(), sqle);
			numberOfOfflineMessages = sqle.getMessage();
		} finally {
			DbConnectionManager.closeConnection(rs, pstmt, databaseConnection);
		}
		return Integer.valueOf(numberOfOfflineMessages);
	}

	public String getSenderFirstName(int userRole, long usreId) {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String firstName = "";
		try {
			databaseConnection = DbConnectionManager.getConnection();
			if (userRole == 1) {
				// to a traveller
				pstmt = databaseConnection
						.prepareStatement(LOAD_SENDER_TRAVELLER_NAME);
			} else {
				// to a guide
				pstmt = databaseConnection
						.prepareStatement(LOAD_SENDER_GUIDE_NAME);
			}
			pstmt.setString(1, usreId + ""); // load the id
			rs = pstmt.executeQuery();
			if (rs.next()) {
				firstName = rs.getString(1);
			}
		} catch (SQLException sqle) {
			Log.error(sqle.getMessage(), sqle);
		} finally {
			DbConnectionManager.closeConnection(rs, pstmt, databaseConnection);
		}

		return firstName;
	}

	public boolean isMessageAlertOn(int userRole, long userId) {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String messageAlert = "1";

		try {
			databaseConnection = DbConnectionManager.getConnection();
			if (userRole == 1) {
				// a traveller
				pstmt = databaseConnection
						.prepareStatement(LOAD_TRAVELLER_MESSAGE_ALERT);
			} else {
				// one of them are not travellers
				pstmt = databaseConnection
						.prepareStatement(LOAD_GUIDE_MESSAGE_ALERT);
			}
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				messageAlert = rs.getString(1);
			}
		} catch (SQLException sqle) {
			Log.error(sqle.getMessage(), sqle);
		} finally {
			DbConnectionManager.closeConnection(rs, pstmt, databaseConnection);
		}

		if (Integer.valueOf(messageAlert) == 1)
			return true;
		else
			return false;
	}

	public boolean isMatchFriend(int senderRole, long senderId, int targetRole,
			long targetId) {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String numberOfMatchFriend = "0";

		try {
			databaseConnection = DbConnectionManager.getConnection();
			if (senderRole == 1 && targetRole == 1) {
				// both are travellers
				pstmt = databaseConnection.prepareStatement(IS_TTMATCH_FRIEND);
				if (senderId > targetId) {
					pstmt.setLong(1, senderId);
					pstmt.setLong(2, targetId);
				} else {
					pstmt.setLong(1, targetId);
					pstmt.setLong(2, senderId);
				}
			} else {
				// one of them are not travellers
				pstmt = databaseConnection.prepareStatement(IS_TGMATCH_FRIEND);
				if (senderRole == 0 && targetRole == 1) {
					pstmt.setLong(1, senderId);
					pstmt.setLong(2, targetId);
				} else {
					pstmt.setLong(1, targetId);
					pstmt.setLong(2, senderId);
				}
			}
			rs = pstmt.executeQuery();
			if (rs.next()) {
				numberOfMatchFriend = rs.getString(1);
			}

		} catch (SQLException sqle) {
			Log.error(sqle.getMessage(), sqle);
		} finally {
			DbConnectionManager.closeConnection(rs, pstmt, databaseConnection);
		}
		if (Integer.valueOf(numberOfMatchFriend) == 1)
			return true;
		else
			return false;
	}

	
	/**
	 * Given targeted device mobile version 
	 */
	public int getMobileVersion(int targetRole, long targetId) {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		int mobileVersionCode = -1;
		try {
			databaseConnection = DbConnectionManager.getConnection();
			if (targetRole == 1) {
				// to a traveller
				pstmt = databaseConnection
						.prepareStatement(LOAD_TRAVELLER_MOBILE_VERSION);
			} else if (targetRole == 0) {
				// to a guide
				pstmt = databaseConnection.prepareStatement(LOAD_GUIDE_MOBILE_VERSION);
			}
			pstmt.setLong(1, targetId); // load the id

			rs = pstmt.executeQuery();
			if (rs.next()) {
				mobileVersionCode = rs.getInt(1);
			}
		} catch (SQLException sqle) {
			Log.error(sqle.getMessage(), sqle);
		} finally {
			DbConnectionManager.closeConnection(rs, pstmt, databaseConnection);
		}
		return mobileVersionCode;
	}
	
	/**
	 * Given targeted device token excluding the mobile version
	 * if you need mobile version, you need modify the sql
	 */
	public UserLoginDevice getDevice(int targetRole, long targetId) {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		UserLoginDevice device = new UserLoginDevice();
		device.setOsType(OsType.ANDROID); // by default
		String deviceToken = null;
		String osType = null;
		try {
			databaseConnection = DbConnectionManager.getConnection();

			if (targetRole == 1) {
				// to a traveller
				pstmt = databaseConnection
						.prepareStatement(LOAD_TRAVELLER_TOKEN);
			} else if (targetRole == 0) {
				// to a guide
				pstmt = databaseConnection.prepareStatement(LOAD_GUIDE_TOKEN);
			}
			pstmt.setLong(1, targetId); // load the id

			rs = pstmt.executeQuery();
			if (rs.next()) {
				osType = rs.getString(1);
				deviceToken = rs.getString(2);
				device.setDeviceToken(deviceToken);
				if (Integer.valueOf(osType) == OsType.IOS.getNumVal()) {
					device.setOsType(OsType.IOS); // it is an IOS
				}
			}

		} catch (SQLException sqle) {
			Log.error(sqle.getMessage(), sqle);
		} finally {
			DbConnectionManager.closeConnection(rs, pstmt, databaseConnection);
		}
		return device;
	}


	/**
	 * After the server send out the message save a copy of the message
	 */
	public void saveMessageAfterSendOut(Message message) {

		PreparedStatement preparedStatement = null;
		
		try {
			databaseConnection = DbConnectionManager.getConnection();
			String addToTableQuery = "insert into IMMessage(packetId, time, fromUsername, toUsername, body) values (?,?,?,?,?)";
			preparedStatement = databaseConnection
					.prepareStatement(addToTableQuery);

			preparedStatement.setString(1, message.getID());
			preparedStatement.setString(2, "" + new Date().getTime());
			preparedStatement.setString(3, message.getFrom().toString());
			preparedStatement.setString(4, message.getTo().toString());
			preparedStatement.setString(5, message.getBody());
			preparedStatement.executeUpdate();
			
		} catch (Exception exception) {
			Log.error(exception.getMessage(), exception);
		} finally {
			DbConnectionManager.closeConnection(preparedStatement, databaseConnection);
		}
	}

	/**
	 * Delete a message from server after receiving a client message receipt
	 */
	public void deleteMessageWithReceiptRecieved(String packetId, String fromUsername) {
		PreparedStatement preparedStatement = null;
		try {
			databaseConnection = DbConnectionManager.getConnection();
			
			String deleteFromTableQuery = "delete from IMMessage where packetId = ? AND fromUsername = ? ";
			preparedStatement = databaseConnection
					.prepareStatement(deleteFromTableQuery);
			preparedStatement.setString(1, packetId);
			preparedStatement.setString(2, fromUsername);
			preparedStatement.executeUpdate();
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			DbConnectionManager.closeConnection(preparedStatement, databaseConnection);
		}
	}

}
