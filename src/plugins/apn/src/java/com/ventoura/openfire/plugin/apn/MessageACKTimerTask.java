package com.ventoura.openfire.plugin.apn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.TimerTask;

import org.jivesoftware.database.DbConnectionManager;
import org.jivesoftware.openfire.MessageRouter;
import org.jivesoftware.openfire.XMPPServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.Message;

import java.sql.ResultSet;

/**
 * This is a alternative solution for the server ack of XEP-198 1. The server
 * check the message table every 45 seconds 2.
 */
public class MessageACKTimerTask extends TimerTask {

	private static final Logger Log = LoggerFactory.getLogger(APNPlugin.class);
	private Connection databaseConnection = null;

	@Override
	public void run() {
		String deleteFromTableQuery = "";
		PreparedStatement selectStatement = null, deleteStatement = null;

		try {
			databaseConnection = DbConnectionManager.getConnection();
			/*
			 * More lost message ?
			 */
			String getAllLostMessage = "SELECT * FROM IMMessage WHERE time < ?";
			selectStatement = databaseConnection
					.prepareStatement(getAllLostMessage);
			selectStatement.setLong(1,
					(new Date().getTime() - APNPlugin.RESEND_TIME_PERIOD_MS));

			ResultSet resultSet = selectStatement.executeQuery();
			String packetId, fromUsername, toUsername, message;

			while (resultSet.next()) {
				packetId = resultSet.getString(1);
				fromUsername = resultSet.getString(3);
				toUsername = resultSet.getString(4);
				message = resultSet.getString(5);



				/*
				 * delete this record after resending out
				 */
				deleteFromTableQuery += "delete from IMMessage where packetId = ? AND fromUsername = ? ; ";
				deleteStatement = databaseConnection
						.prepareStatement(deleteFromTableQuery);
				deleteStatement.setString(1, packetId);
				deleteStatement.setString(2, fromUsername);
				deleteStatement.executeUpdate();

				/*
				 * resend the lost message
				 */
				MessageRouter router = XMPPServer.getInstance()
						.getMessageRouter();
				Message resendMessage = new Message();
				resendMessage
						.setSubject(APNPlugin.MESSAGE_SUBJECT_SERVER_MESSAGE_LOST_RESEND);
				resendMessage.setTo(toUsername);
				resendMessage.setFrom(fromUsername);
				resendMessage.setID(packetId);
				resendMessage.setType(Message.Type.chat);
				resendMessage.setBody(message);
				router.route(resendMessage);


			}

		} catch (Exception exception) {
			Log.error(exception.getMessage(), exception);
		} finally {
			DbConnectionManager.closeStatement(deleteStatement);
			DbConnectionManager.closeConnection(selectStatement,
					databaseConnection);
		}

	}
};
