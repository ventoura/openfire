package com.ventoura.openfire.plugin.apn;

public enum OsType {

	IOS(0), ANDROID(1);

	private int numVal;

	OsType(int numVal) {
		this.numVal = numVal;
	}

	public int getNumVal() {
		return numVal;
	}

	public void setNumVal(int numVal) {
		this.numVal = numVal;
	}
}
