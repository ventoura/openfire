package com.ventoura.openfire.plugin.apn;

import java.io.File;
import java.util.Timer;

import javapns.notification.PushNotificationPayload;

import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.interceptor.InterceptorManager;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.jivesoftware.util.JiveGlobals;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;

public class APNPlugin implements Plugin, PacketInterceptor {

	private static final Logger Log = LoggerFactory.getLogger(APNPlugin.class);

	/*
	 * XEP-198 solution
	 */
	Timer timer;
	public static final long EXECUTE_INTERVAL_MS = 30000L;
	public static final long RESEND_TIME_PERIOD_MS = 45000L;

	private static final String IM_SERVER_ACCOUNT = "im@ventoura.com";

	public static final String MESSAGE_SUBJECT_CLIENT_MESSAGE_RECEIPT = "CLT_MEG_RPT";
	public static final String MESSAGE_SUBJECT_SERVER_MESSAGE_RECEIPT = "MESSAGE_RECEIPT";
	public static final String MESSAGE_SUBJECT_SERVER_MESSAGE_LOST_RESEND = "MSG_RSD";

	private InterceptorManager interceptorManager;
	private ApnDBHandler dbManager;

	public APNPlugin() {
		interceptorManager = InterceptorManager.getInstance();
		dbManager = new ApnDBHandler();
		// And From your main() method or any other method
		timer = new Timer();
		timer.schedule((new MessageACKTimerTask()), 0L, EXECUTE_INTERVAL_MS);
	}

	@Override
	public void initializePlugin(PluginManager pManager, File pluginDirectory) {
		interceptorManager.addInterceptor(this);
	}

	@Override
	public void destroyPlugin() {
		if (timer != null) {
			timer.cancel();
		}
		interceptorManager.removeInterceptor(this);
	}

	/********************************************
	 * Functions supporting web *
	 *********************************************/
	public void setKeyStorePath(String keyStorePath) {
		JiveGlobals.setProperty("plugin.apn.keystorepath", keyStorePath);
	}

	public String getKeystorePath() {
		return JiveGlobals.getProperty("plugin.apn.keystorepath", "");
	}

	public void setPassword(String password) {
		JiveGlobals.setProperty("plugin.apn.password", password);
	}

	public String getPassword() {
		return JiveGlobals.getProperty("plugin.apn.password", "");
	}

	public void setProduction(String production) {
		JiveGlobals.setProperty("plugin.apn.production", production);
	}

	public boolean getProduction() {
		return Boolean.parseBoolean(JiveGlobals.getProperty(
				"plugin.apn.production", "false"));
	}

	/********************************************
	 * Functions for supporting APN
	 *********************************************/
	public int getBadge(JID jid) {
		return dbManager.getNumberOfOfflineMessage(jid);
	}

	public void setSound(String sound) {
		JiveGlobals.setProperty("plugin.apn.sound", sound);
	}

	public String getSound() {
		return JiveGlobals.getProperty("plugin.apn.sound", "default");
	}

	/*
	 * This function will be called three twice if the user is offline and three
	 * times if the user is online. 1. Processed false, incoming true 2. Process
	 * true, incoming true When send out the message: 3. Process false, incoming
	 * false
	 * 
	 * @see
	 * org.jivesoftware.openfire.interceptor.PacketInterceptor#interceptPacket
	 * (org.xmpp.packet.Packet, org.jivesoftware.openfire.session.Session,
	 * boolean, boolean)
	 */
	@Override
	public void interceptPacket(Packet packet, Session session,
			boolean incoming, boolean processed) throws PacketRejectedException {
		/*
		 * handle push notification
		 */
		if (packet instanceof Message) {

			Message receivedMessage = (Message) packet;
			if (receivedMessage.getType() != Message.Type.chat) {
				return;
			}

			String toUsername = receivedMessage.getTo().toBareJID().split("@")[0];
			String fromUsername = receivedMessage.getFrom().toBareJID()
					.split("@")[0];

			
			if (fromUsername.equals("ventourateam") || fromUsername.equals("ventouraserver")) {
				/*
				 * ignore other messages not sent from ventoura mobile,
				 * also ignored the message sent from server
				 */
				return;
				
			} else if (!processed && incoming) {

				if (isClientReceipt(receivedMessage)) {

					dbManager.deleteMessageWithReceiptRecieved(receivedMessage
							.getID(), receivedMessage.getTo().toString());
					/*
					 * receipt need to be rejected
					 */
					PacketRejectedException rejected = new PacketRejectedException(
							"Receipt");
					throw rejected;

				} else if (isServerReceipt(receivedMessage)) {
					/*
					 * Message receipt from server
					 */
					return;

				} else {
					/*
					 * Normal message from client
					 */
					String[] senderValues = fromUsername.split("_");
					int senderUserRole = senderValues[0].equals("t") ? 1 : 0;
					long senderUserId = Long.valueOf(senderValues[1]);
					String[] targetValues = toUsername.split("_");
					int targetUserRole = targetValues[0].equals("t") ? 1 : 0;
					long targetUserId = Long.valueOf(targetValues[1]);

					/*
					 * send back message receipt
					 */
					sendBackServerReceipt(receivedMessage.getFrom(),
							receivedMessage.getID());

					/*
					 * handle the message content
					 */
					if (!dbManager.isMatchFriend(senderUserRole, senderUserId,
							targetUserRole, targetUserId)) {
						/*
						 * They are not friends, just throw an exception, the
						 * user will not receive this message
						 */
						PacketRejectedException rejected = new PacketRejectedException(
								"They are not friend");
						throw rejected;
					} else {
						/*
						 * they are friends
						 */
						if (receivedMessage.getSubject() == null
								|| !receivedMessage
										.getSubject()
										.equals(MESSAGE_SUBJECT_SERVER_MESSAGE_LOST_RESEND)) {
							String body = receivedMessage.getBody();
							try {

								UserLoginDevice device = dbManager.getDevice(
										targetUserRole, targetUserId);
								/*
								 * 1. if the device is not saved in the
								 * database, no notification is sent 2. only
								 * send the notification when the message alert
								 * is on
								 */
								if (device != null
										&& device.getOsType() == OsType.IOS
										&& dbManager.isMessageAlertOn(
												targetUserRole, targetUserId)) {

									sendIOSPushNotification(senderUserRole,
											senderUserId, body,
											device.getDeviceToken());

								}
							} catch (Exception e) {
								Log.error(e.getMessage(), e);
							}
						}
					}
				}

			} else if (!processed && !incoming) {
				if (!isServerReceipt(receivedMessage)
						&& !isClientReceipt(receivedMessage)) {

					try {
						String[] targetValues = receivedMessage.getTo()
								.toBareJID().split("@")[0].split("_");

						int targetUserRole = -1;
						if (targetValues[0].equals("t")) {
							targetUserRole = 1;
						} else if (targetValues[0].equals("g")) {
							targetUserRole = 0;
						}
						if (targetUserRole != -1) {
							long targetUserId = Long.valueOf(targetValues[1]);
							if (dbManager.getMobileVersion(targetUserRole,
									targetUserId) > 0) {
								dbManager
										.saveMessageAfterSendOut(receivedMessage);
							}
						}
					} catch (Exception e) {
						Log.error(e.getMessage());
					}
				}
			}
		}
	}

	private void sendIOSPushNotification(int senderUserRole, long senderUserId,
			String messageBody, String deviceToken) throws JSONException {

		PushNotificationPayload payload = PushNotificationPayload.complex();
		payload.addAlert(dbManager.getSenderFirstName(senderUserRole,
				senderUserId) + ": " + messageBody);

		// payload.addBadge(getBadge(targetJID));
		payload.addBadge(1);
		payload.addSound("default");
		payload.addCustomDictionary("notificationType", 1);
		payload.addCustomDictionary("senderId", senderUserId + "");
		payload.addCustomDictionary("senderRole", senderUserRole + "");
		/*
		 * send the ios push notification
		 */
		new PushMessage(payload, getKeystorePath() + "/iosKeyStore.p12",
				getPassword(), getProduction(), deviceToken).start();

	}

	private void sendBackServerReceipt(JID to, String packetId) {
		Message messageReceipt = new Message();
		messageReceipt.setType(Message.Type.chat);
		messageReceipt.setSubject(MESSAGE_SUBJECT_SERVER_MESSAGE_RECEIPT);
		messageReceipt.setTo(to);
		messageReceipt.setFrom(IM_SERVER_ACCOUNT);
		messageReceipt.setID(packetId);
		XMPPServer.getInstance().getMessageRouter().route(messageReceipt);
	}

	private boolean isServerReceipt(Message receivedMessage) {
		return receivedMessage.getSubject() != null
				&& receivedMessage.getSubject().equals(
						MESSAGE_SUBJECT_SERVER_MESSAGE_RECEIPT);

	}

	private boolean isClientReceipt(Message receivedMessage) {
		return receivedMessage.getSubject() != null
				&& receivedMessage.getSubject().equals(
						MESSAGE_SUBJECT_CLIENT_MESSAGE_RECEIPT);
	}
}
