package com.ventoura.openfire.plugin.apn;

public class  UserLoginDevice{

	private OsType osType; // 0 represents IOS, 1 represents Android

	private String deviceToken;
	
	private int mobileVersionCode;
	
	public int getMobileVersionCode() {
		return mobileVersionCode;
	}

	public OsType getOsType() {
		return osType;
	}

	public void setOsType(OsType osType) {
		this.osType = osType;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
}
