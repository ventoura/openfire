package com.ventoura.openfire.plugin.apn;

import javapns.Push;
import javapns.notification.PushNotificationPayload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PushMessage extends Thread {

    private static final Logger Log = LoggerFactory.getLogger(APNPlugin.class);

    private Object keystore;
    private String password;
    private PushNotificationPayload payload;
    private boolean production;
    private String deviceToken;

    public PushMessage(PushNotificationPayload payload, Object keystore, String password, boolean production, String deviceToken) {
        this.payload = payload;
        this.keystore = keystore;
        this.password = password;
        this.production = production;
        this.deviceToken = deviceToken;
    }

    public void run() {
        try {
        	Push.payload(payload, keystore, password, production, deviceToken);
        } catch (Exception e) {
            Log.error(e.getMessage(), e);
        }
    }
}
